import datetime

#Tomar la fecha actual
fecha = datetime.datetime.today()
print(f"La fecha de hoy es {fecha.date()}")

def ageToSeconds():
    #Tomar la fecha de nacimiento
    nacimiento = input("Inserte fecha de nacimiento (YYYY-MM-DD)\n")
    try:
        nacimiento = datetime.datetime.strptime(nacimiento, '%Y-%m-%d')
    except ValueError:
        print("Error. Inserte la fecha como corresponde")
        exit()
    #Calcular la diferencia
    delta = fecha - nacimiento
    print(f"Tu vida ha pasado por {delta.days} dias",)
    #Pasar diferencia a segundos
    tiempo = ((int(delta.days)) * 86400)
    print(f"Tu vida en segundos ha sido: {tiempo} segundos.")

ageToSeconds()
respuesta = input("Desea consultar con otra fecha? Y/N \n")

while 1>0:
    if respuesta.lower() == "y":
        ageToSeconds()
        respuesta = input("Desea consultar con otra fecha? Y/N \n")
    elif respuesta.lower() == "n":
        break
    else:
        respuesta = input("Inserte una respuesta valida. \nDesea consultar con otra fecha? Y/N \n")