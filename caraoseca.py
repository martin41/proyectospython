import random
moneda = ("cara", "seca")

def jogo():
    participante = input("Elija Cara o Seca: \n" )
    while participante.lower() not in moneda:
        participante = input("Inserte una respuesta valida. \nCara o Seca?\n")
    resultado = random.choice(moneda)
    print("El resultado es: {}".format (resultado.title()))
    if resultado == participante:
        print("HAS GANADO!")
    else:
        print("Has perdido.")

respuesta = input("Desea jugar al cara o seca? Y/N \n")

while 1>0:
    if respuesta.lower() == "y":
        jogo()
        respuesta = input("Desea jugar otra vez? Y/N \n")
    elif respuesta.lower() == "n":
        break
    else:
        respuesta = input("Inserte una respuesta valida. \nDesea jugar al cara o seca? Y/N \n")