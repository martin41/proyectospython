import random
name = (["Adrian", "Alejo", "Benjamin", "Carlos", "Dario", "Ezequiel", "Francisco", "Gabriel", "Horacio", "Ignacio", "Joaquin", "Kaka", "Lautaro", "Martin", "Nazareno", "Obligado", "Pascual" , "Quico", "Roberto", "Solomon", "Tobias"])
surname = (["Atenas", "Barzan", "Canoza", "Daedalo", "Esther", "Fanobio", "Gomez", "Habisco", "Iblis", "Jordania", "Kariso", "Lazaro", "Maizena", "Nascar", "Ostento", "Perez", "Quincena", "Ras", "Solbon", "Taras"])

answer = input("Desea recibir un nombre aleatorio? \n" )

while isinstance(answer, str):
    if answer.lower() == "yes":
        nombre = random.choice(name)
        apellido = random.choice(surname)
        print ("Tu nombre aleatorio es: {} {}".format (nombre, apellido))
        answer = input('Desea otro nombre aleatorio? \n')
    elif answer.lower() == "no":
        break
    else:
        answer = input('Inserte una respuesta valida. Desea otro nombre aleatorio? \n')
